package location

// Location represents a real life location. It is a linear array of
// a latitude and a longitude. Precision is limited and satisfied to
// practical requirements of real life applications.
type Location [2]float32

// IsValid returns the validity of a give location. It checks for range
// of the location primarily. Based on information from
// https://docs.mongodb.com/manual/geospatial-queries/#legacy-coordinate-pairs
func (location Location) IsValid() bool {
	latitude, longitude := location[0], location[1]

	if latitude < -180 && latitude > 180 {
		return false
	}

	if longitude < -90 && longitude > 90 {
		return false
	}

	return true
}
