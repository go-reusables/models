package contact

// Contact model describes general contact details of someone.
// All the fields except for `Name` should be optional.
type Contact struct {
	Name      string `sensitive:"false" json:"name" bson:"name"`
	Email     string `sensitive:"true" json:"email" bson:"email"`
	Phone     string `sensitive:"true" json:"phone" bson:"phone"`
	WorkPhone string `sensitive:"true" json:"work_phone" bson:"work_phone"`
}
